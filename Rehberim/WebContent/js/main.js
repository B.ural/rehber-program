var getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1), sURLVariables = sPageURL
			.split('&'), sParameterName, i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return typeof sParameterName[1] === undefined ? true
					: decodeURIComponent(sParameterName[1]);
		}
	}
	return false;
};

function ekle() {
	var rehber = new Object();
	rehber.name = $('#name').val();
	rehber.surname = $('#surname').val();
	rehber.number = $('#number').val();

	var rehberAsJson = JSON.stringify(rehber);
	if (rehber.name == "" || rehber.surname == "" || rehber.number == "") {
		alert("Bo� girilemez");
	} else {
		$.ajax({
			type : "POST",
			url : "http://localhost:8081/Rehberim/InsertRehber",
			data : rehberAsJson,

			success : function(response) {
				alert("Kayit eklendi");
				location.reload();
			}
		});
	}
}
function tabloGoster() {

	$
			.ajax({
				type : "GET",
				url : "http://localhost:8081/Rehberim/FindRehberList",
				success : function(response) {

					var rehberList = JSON.parse(response);
					for (var int = 0; int < rehberList.length; int++) {
						var rehber = rehberList[int];
						$("#customers")
								.append(
										"<tr><td>"
												+ rehber.id
												+ "</td> <td> "
												+ rehber.name
												+ "</td> <td> "
												+ rehber.surname
												+ "</td><td> "
												+ rehber.number
												+ "</td> <td> <input type='submit'  name='Sil' id='id' value='Sil' onclick='kisiSil("
												+ rehber.id
												+ ")'</td> "
												+ "</td> <td> <input type='submit'  name='Duzenle' id='id' value='Duzenle' onclick='kisiDuzenle("
												+ rehber.id + ")'</td> </tr>");

					}
				}
			});
}
function kisiSil(id) {

	$.ajax({
		type : "GET",
		url : "http://localhost:8081/Rehberim/DeleteRehber",
		data : 'id=' + id,
		success : function(data) {

			alert("Kisi silindi");
			location.reload();
		}
	});
}

function kisiDuzenle(id) {
	window.location.href = "http://localhost:8081/Rehberim/kisiduzenle.html?id="
			+ id;
}

function kisiDoldur() {
	var id = getUrlParameter('id');
	console.log(id);
	$.ajax({
		type : "GET",
		url : "http://localhost:8081/Rehberim/FindRehber?id=" + id,
		success : function(response) {
			var rehber = JSON.parse(response);
			$("#id")[0].value = rehber.id;
			$("#name")[0].value = rehber.name;
			$("#surname")[0].value = rehber.surname;
			$("#number")[0].value = rehber.number;
		}
	});

}

function rehberGuncelle() {
	var rehber = {
		id : $("#id")[0].value,
		name : $("#name")[0].value,
		surname : $("#surname")[0].value,
		number : $("#number")[0].value
	};

	var rehberAsJson = JSON.stringify(rehber);

	if (rehber.name == "" || rehber.surname == "" || rehber.number == "") {
		alert("Bo� girilemez");
	} else {
	$.ajax({
				type : "POST",
				url : "http://localhost:8081/Rehberim/UpdateRehber",
				data : rehberAsJson,
				success : function(response) {
					if (response == "BASARILI") {
						window.location = "http://localhost:8081/Rehberim/table.html";
					} else {
						alert("HATA ALINDI");
					}
				}
			});
}
}
function anaMenuYonlendir() {
	window.location = "http://localhost:8081/Rehberim/";
}