package com.uniyaz.rehber.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.uniyaz.rehber.domain.Rehber;

public class FindRehberListServlet extends HttpServlet {
	Connection con;
	PreparedStatement pst;
	ResultSet rs;
	int row;

	public void doGet(HttpServletRequest req, HttpServletResponse rsp)
			throws IOException, ServletException {
		rsp.setContentType("text/html");
		PrintWriter out = rsp.getWriter();

		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/rehber", "root", "1234");
			String sql;
			sql = "select *from rehber";
			Statement stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			
			List<Rehber> rehberList = new ArrayList<Rehber>();
			
			while (rs.next()) {
				Rehber rehber = new Rehber();
				rehber.setId(rs.getInt("id"));
				rehber.setName(rs.getString("name"));
				rehber.setSurname(rs.getString("surname"));
				rehber.setNumber(rs.getString("number"));
				rehberList.add(rehber);
			}
			Gson gson = new Gson();
			String json = gson.toJson(rehberList);
			out.write(json);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
