package com.uniyaz.rehber.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.uniyaz.rehber.domain.Rehber;

public class UpdateRehberServlet extends HttpServlet {
	Connection con;
	PreparedStatement pst;
	ResultSet rs;
	int row;

	public void doPost(HttpServletRequest req, HttpServletResponse rsp)
			throws IOException, ServletException {
		rsp.setContentType("text/html");
		PrintWriter out = rsp.getWriter();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/rehber", "root", "1234");

			StringBuffer stringBuffer = new StringBuffer();
			String line = null;
			BufferedReader bufferedReader = req.getReader();
			while ((line = bufferedReader.readLine()) != null) stringBuffer.append(line);

			String rehberAsJson = stringBuffer.toString();
			Gson gson = new Gson();
			Rehber rehber = gson.fromJson(rehberAsJson, Rehber.class);

			String sql = "UPDATE rehber SET name = ? , surname = ? , number = ? WHERE id = ?";
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, rehber.getName());
			statement.setString(2, rehber.getSurname());
			statement.setString(3, rehber.getNumber());
			statement.setInt(4, rehber.getId());
			int rowsInserted = statement.executeUpdate();
			out.write("BASARILI");

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
